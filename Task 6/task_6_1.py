# 1)	Создать матрицу случайных чисел от a до b, размерность матрицы n*m
# 2)	Найти максимальный элемент матрицы.
# 3)	Найти минимальный минимальный матрицы.
# 4)	Найти сумму всех элементов матрицы.
# 5)	Найти индекс ряда с максимальной суммой элементов.
# 6)	Найти индекс колонки с максимальной суммой элементов.
# 7)	Найти индекс ряда с минимальной суммой элементов.
# 8)	Найти индекс колонки с минимальной суммой элементов.
# 9)	 Обнулить все элементы выше главной диагонали.
# 10)	 Обнулить все элементы ниже главной диагонали.
# 11)	 Создать две новые матрицы matrix_a, matrix_b случайных чисел размерностью n*m.
# 12)	Создать матрицу равную сумме matrix_a и matrix_b.
# 13)	Создать матрицу равную разности matrix_a и matrix_b.
# 14)	Создать новую матрицу равную matrix_a умноженной на g. g вводится с клавиатура
import random

a = 1
b = 9
n, m = 3, 3
# ------ 1 ------
# matrix=[[random.randint(a,b) for i in range(n)] for j in range(m)]
# print(matrix)

# ------ 2 ------
# print(max([max(matrix[i]) for i in range(m)]))

# ------ 3 ------
# print(min([min(matrix[i]) for i in range(m)]))

# ------ 4 ------
# print(sum([sum(matrix[i]) for i in range(m)]))

# ------ 5 ------
# lst_row=[sum(matrix[i]) for i in range(m)]
# print(lst_row.index(max(lst_row)))

# ------ 6 ------
# new_matrix=[[matrix[i][j] for i in range(m)] for j in range(n)]
# lst_col=[sum(new_matrix[i]) for i in range(n)]
# print(lst_col.index(max(lst_col)))

# ------ 7 ------
# print(lst_row.index(min(lst_row)))

# ------ 8 ------
# print(lst_col.index(min(lst_col)))

# ------ 9 ------
# for i in range(m):
#    for j in range(n):
#        if j>i:matrix[i][j]=0
# print(matrix)

# ------ 10 ------
# for i in range(m):
#    for j in range(n):
#        if j<i:matrix[i][j]=0
# print(matrix)

# ------ 11 ------
matrix_a = [[random.randint(a, b) for j in range(n)] for i in range(m)]
# matrix_b = [[random.randint(a, b) for j in range(n)] for i in range(m)]
# print(matrix_a, matrix_b, sep='\n')

# ------ 12 ------
# matrix_sum=[[matrix_a[i][j]+matrix_b[i][j] for j in range(n)] for i in range(m)]
# print(matrix_sum)

# ------ 13 ------
# matrix_def=[[matrix_a[i][j]-matrix_b[i][j] for j in range(n)] for i in range(m)]
# print(matrix_def)

# ------ 14 ------
g = int(input())
matrix_mult = [[matrix_a[i][j] * g for j in range(m)] for i in range(n)]
print(matrix_mult)
