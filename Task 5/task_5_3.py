# Два натуральных числа называют дружественными, если каждое из них
# равно сумме всех делителей другого, кроме самого этого числа. Найти все
# пары дружественных чисел, лежащих в диапазоне от 200 до 300.

# Функция для вычис
def sum_dev(num):
    list_dev = []
    for j in range(1, num):
        if num % j == 0:
            list_dev.append(j)
    return sum(list_dev)

friends_list=[]
for i in range(200, 300):
    s = sum_dev(i)
    if sum_dev(s) == i: #and i <= s:
        friends_list.append((i, s))
print(friends_list)