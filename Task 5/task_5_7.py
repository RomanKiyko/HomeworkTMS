# Дана целочисленная квадратная матрица. Найти в каждой строке наибольший
# элемент и поменять его местами с элементом главной диагонали.
matrix = [[1, 2, 3],
          [4, 5, 6],
          [8, 9, 10]]
for i in range(len(matrix)):
    maxi = max(matrix[i])
    for j in range(len(matrix)):
        if matrix[i][j]==maxi:
            matrix[i][j]=matrix[i][i]
            matrix[i][i]=maxi
print(matrix)
