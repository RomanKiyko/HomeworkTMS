# Для каждого натурального числа в промежутке от m до n вывести все делители, кроме единицы и самого числа.
# m и n вводятся с клавиатуры.
# m,n=int(input()),int(input())
def dividers(x,y):
    lst_dev=[]
    Dict={}
    for i in range(x,y+1):
        for j in range(2, i):
            if i % j == 0:
                lst_dev.append(j)
            Dict[i]=lst_dev
        lst_dev = []
    return Dict
print(dividers(100,105))
