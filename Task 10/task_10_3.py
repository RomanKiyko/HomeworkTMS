import csv
import datetime

date = [[1, 3, 2021], [30, 6, 1943],[12,9,1966],[29,3,1982]]
with open('date_file.csv', 'w', newline='', encoding='utf-8') as f:
    f_write = csv.writer(f)
    f_write.writerows(date)

with open('date_file.csv', newline='', encoding='utf-8') as f:
    f_read = csv.reader(f)
    lst_date=[]
    for row in f_read:
        lst_date.append(datetime.date(int(row[2]),int(row[1]),int(row[0])))
    print(min(lst_date))
