import csv

data = [['1.05.21', 'Minsk', 13, 2], ['2.05.21', 'Minsk', 14, 4], ['3.05.21', 'Minsk', 5, 5],
        ['4.05.21', 'Minsk', 9, 4], ['5.05.21', 'Minsk', 15, 3], ['6.05.21', 'Minsk', 11, 5]]
with open('weather.csv', 'w',newline='',encoding='utf-8') as f:
    name_row = ['Date', 'City', 'Temp', 'Wind']
    f_write = csv.writer(f)
    f_write.writerow(name_row)
    f_write.writerows(data)
with open('weather.csv',newline='',encoding='utf-8') as f:
    print(f.read())
with open('weather.csv',newline='',encoding='utf-8') as f:
    f_read = csv.reader(f)
    i, t, w, = 0, 0, 0
    for row in f_read:
        if row[1] == 'Minsk' and i < 3:
            t += int(row[2])
            w += int(row[3])
            i += 1
    print(t / 3, w / 3)
