import csv

data = [['Ivan', 'Bulkin', 15], ['Oksana', 'Fadeeva', 23], ['Fedor', 'Volkov', 42], ['Sergey', 'Mozaev', 31],
        ['Vlad', 'Borsukov', 29], ['Vlad', 'Stotskiy', 19]]
with open('ages.csv', "w",newline='',encoding='utf-8') as f:
    f_names = ['Firstname', 'Lastname', 'Age']
    file_write = csv.writer(f)
    file_write.writerows(data)
with open('ages.csv',newline='',encoding='utf-8') as f:
    print(f.read())
with open('ages.csv',newline='',encoding='utf-8') as f:
    i1, i2, i3, i4, i5 = 0, 0, 0, 0, 0
    read = csv.reader(f)
    for row in read:
        if int(row[2]) in range(1, 13):
            i1 += 1
        if int(row[2]) in range(13, 19):
            i2 += 1
        if int(row[2]) in range(19, 26):
            i3 += 1
        if int(row[2]) in range(26, 41):
            i4 += 1
        if int(row[2]) > 40:
            i5 += 1
    with open('deff_ages.csv', 'w',newline='',encoding='utf-8') as f1:
        file_writer1 = csv.writer(f1)
        file_writer1.writerow(['1-12', '13-18', '19-25', '26-40', '40+'])
        file_writer1.writerow([i1, i2, i3, i4, i5])
with open('deff_ages.csv',newline='',encoding='utf-8') as f:
    print(f.read())
