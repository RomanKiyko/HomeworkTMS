# Создать lambda функцию, которая принимает на вход неопределенное количество именных аргументов
# и выводит словарь с ключами удвоенной длины. {‘abc’: 5} -> {‘abcabc’: 5}

def lamb(**kwargs):
    new_dict=(lambda **kwargs: {i * 2: kwargs[i] for i in kwargs})(**kwargs)
    return new_dict

print(lamb(qwer='qwer', list=[1, 2, 3], int=123, float=1.2, tuple=(1, 2, 3),dict={1: 2, 2: 1}))
