# Создать универсальный декоратор, который меняет порядок аргументов в функции на противоположный.

def decorator_reverse_args(decorating_function):
    def rev(*args):
        if len(args)>1:
            reverse=args[::-1]
            return reverse
        else:
            return args[0]
    return rev
@decorator_reverse_args
def decorating_function(*args):
    return args

print(decorating_function(0))