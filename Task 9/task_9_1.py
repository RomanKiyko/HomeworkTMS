# Дан список строк. Отформатировать все строки в формате ‘{i} - {string}’,
# где i это порядковый номер строки в списке. Использовать генератор списков.

def form(lst):
    lst=list(enumerate(lst))
    lst_form = [f'{(i+1)} - {j}' for i,j in lst]
    return lst_form

lst_str=[4 - 3, 2, '3', 3 + 1, '5', 6, 7]
print(form(lst_str))