# Создать декоратор для функции, которая принимает список чисел.
# Декоратор должен производить предварительную проверку данных - удалять все четные элементы из списка.
lst=[2, 4, 5, 7, 0, 6, 8, 11, 22, 34]


# def decorator(numbers,lst):
#     new_lst = [i for i in numbers(lst) if i % 2 != 0]
#     return numbers(new_lst)
#
# def numbers(lst):
#     return lst

def decorator(numbers):
    def wrapper(lst):
        new_lst = [i for i in lst if i % 2 != 0]
        return numbers(new_lst)
    return wrapper

@decorator
def numbers(lst):
    return lst

print(numbers(lst))